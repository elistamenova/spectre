import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../auth/auth.service';

/**
 * @title Basic toolbar
 */
@Component({
  selector: 'app-main-nav-component',
  templateUrl: 'main-nav.component.html',
  styleUrls: ['main-nav.component.css'],
})
export class MainNavComponent {
  constructor(private router: Router, private authService: AuthService) {
  }

  isUserAuthenticated(): boolean {
    if (!localStorage.getItem('UserLoggedIn')) {
      return false;
    } else {
      return true;
    }
  }

  onLogout(): void {
    this.authService.logout();
    this.router.navigate(['/login']);
  }
}
