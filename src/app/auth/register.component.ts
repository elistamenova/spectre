import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from './auth.service';
import {Router} from '@angular/router';
import {emailRegx} from '../constants';

@Component({
  selector: 'app-auth',
  templateUrl: './register.component.html',
  styleUrls: ['./auth.component.css']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  isLoading = false;
  error: string = null;

  constructor(private formBuilder: FormBuilder, private authService: AuthService, private router: Router) {
  }

  ngOnInit(): void {
    localStorage.clear(); // clear storage when user navigates to register page
    this.registerForm = this.formBuilder.group({
      email: [null, [Validators.required, Validators.pattern(emailRegx)]],
      password: [null, Validators.required],
      passwordConfirm: [null, Validators.required]
    }, {validator: this.mustMatch('password', 'passwordConfirm')});
  }

  mustMatch(controlName: string, matchingControlName: string): object {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];

      if (matchingControl.errors && !matchingControl.errors.mustMatch) {
        // return if another validator has already found an error on the matchingControl
        return;
      }
      // set error on matchingControl if validation fails
      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({mustMatch: true});
      } else {
        matchingControl.setErrors(null);
      }
    };
  }

  submit(): void {
    if (!this.registerForm.valid) {
      return;
    }
    const email = this.registerForm.value.email;
    const password = this.registerForm.value.password;
    this.isLoading = true;
    // Check if user with this email already exist
    this.authService.checkIfUserExist(email).subscribe(resData => {
      this.isLoading = false;
      // If user is already registered, display error message
      if (resData[0]) {
        this.error = 'This email is already registered.';
      } else {
        this.registerValidUser(email, password);
      }
    }, error => {
      this.isLoading = false;
      this.error = error.message;
    });
  }

  registerValidUser(email: string, password: string): void {
    this.isLoading = true;
    this.authService.register(email, password).subscribe(resData => {
      this.isLoading = false;
      this.router.navigate(['']);
    }, error => {
      this.error = error.message;
      this.isLoading = false;
    });
  }
}
