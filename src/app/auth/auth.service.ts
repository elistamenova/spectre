import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {API} from '../../environments/environment';
import {User} from '../models/user.model';

interface AuthResponseData {
  id: string;
  email: string;
  password: string;
}

@Injectable({providedIn: 'root'})
export class AuthService {
  usersAPI = API + 'users';
  constructor(private http: HttpClient) {
  }

  login(email: string, password: string): Observable<AuthResponseData> {
    return this.http.get<AuthResponseData>(
      this.usersAPI, {params: {email: email, password: password}}
    ).pipe(tap(resData => {
        if (resData[0]) {
          this.handleAuthentication(resData[0].email);
        }
      })
    );
  }

  checkIfUserExist(email: string): Observable<AuthResponseData> {
    return this.http.get<AuthResponseData>(
      this.usersAPI, {params: {email: email}}
    );
  }

  register(email: string, password: string): Observable<AuthResponseData> {
    return this.http.post<AuthResponseData>(
      this.usersAPI, {email: email, password: password}
    ).pipe(tap(resData => {
      this.handleAuthentication(email);
    }));
  }

  logout(): void {
    localStorage.clear();
  }

  private handleAuthentication(email: string): void {
    const user = new User(email);
    localStorage.setItem('UserLoggedIn', JSON.stringify(user));
  }
}
