import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from './auth.service';
import {Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {emailRegx} from '../constants';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit, OnDestroy {
  loginForm: FormGroup;
  isLoading = false;
  error: string = null;
  loginSubscription: Subscription;

  constructor(private formBuilder: FormBuilder, private authService: AuthService, private router: Router) {}

  ngOnInit(): void {
    localStorage.clear(); // clear storage when user navigates to login page
    this.loginForm = this.formBuilder.group({
      email: [null, [Validators.required, Validators.pattern(emailRegx)]],
      password: [null, Validators.required]
    });
  }

  submit(): void {
    if (!this.loginForm.valid) {
      return;
    }
    const email = this.loginForm.value.email;
    const password = this.loginForm.value.password;
    this.isLoading = true;

    this.loginSubscription = this.authService.login(email, password)
      .subscribe(resData => {
        this.isLoading = false;
        if (!resData[0]) {
          this.error = 'Invalid username or password!';
        } else {
          this.error = null;
          this.router.navigate(['']);
        }
      }, error => {
        this.error = error.message;
        this.isLoading = false;
      });
  }

  ngOnDestroy(): void {
    this.loginSubscription.unsubscribe();
  }
}
